This will provide a library to do backup operations on files. It will allow things like rotating logs.

We could also potentially provide easy ways to backup and archive other data within this module.